format = unwritten-1

# vim: set sts=4 sw=4 et ft=conf :
#
# Do not change anything above these lines. When creating a new .conf file,
# copy everything up to and including these lines exactly.
#
# Use this to keep track of general packages we want but don't have written
# yet. Unwritten packages will show up in --query etc, but can't be installed,
# so don't unconditionally depend upon them.
#
# If you're going to add lots of related things, make a new conf file rather
# than shoving everything in here.
#
# Things should be removed from here once they're written. Paludis will
# correctly select written things over anything in here, but it will still show
# the unwritten IDs in query output and so on.
#
# The format is like this:
#
#     cat/
#         pkg/
#             :slot version
#                 key = value
#                 key = value
#
# Careful with whitespace. The indenting matters. Four spaces per level.
#
# Keys are as follows:
#     homepage         One or more URIs, space separated. Required.
#     description      A string. Required.
#     added-by         Name <email>. Required.
#     comment          Why you added it, etc. Recommended.
#     bug-ids          exherbo:1234 , space separated. Optional.
#     remote-ids       freecode:foo , space separated. Optional.

app-arch/
    rar/
        :0 3.90_beta3
            homepage = http://www.rarsoft.com/
            description = Powerful archive manager
            added-by = David Leverton <dleverton@exherbo.org>
            comment = lesspipe can use this
    unar/
        :0 2.7.1
            description = Free uncompression utility for rar files
            comment = Draws in gnustep; Wrote exhereses for it and its deps. The result was so ugly I did not publish it. Contact me if you want to work on it.
app-crypt/
    sbsigntool/
        :0 0.8
            homepage = https://git.kernel.org/cgit/linux/kernel/git/jejb/sbsigntools.git/
            description = Utilities for signing and verifying files for UEFI Secure Boot
            added-by = Bernhard Frauendienst <exherbo@nospam.obeliks.de>
            comment = sys-boot/refind can use this
app-emulation/
    vmware-workstation/
        :0 6.5
            homepage = http://www.vmware.com
            description = A system that allows you to run multiple virtual OSes at the same time.
            added-by = Wulf C. Krueger <philantrop@exherbo.org>
            remote-ids = freecode:vmware
app-spell/
    voikko/
        :0 3.7.1
            homepage = http://voikko.puimula.org/
            description = A spelling and grammar checker and related linguistic data for Finnish language
            added-by = Heiko Becker <heirecka@exherbo.org>
            comment = Optional dep of kde-frameworks/sonnet
app-text/
    dvi2tty/
        :0 5.3.1
            homepage = http://www.ctan.org/tex-archive/dviware/dvi2tty/ ftp://ftp.mesa.nl/pub/dvi2tty/
            description = Previews dvi-files on text-only devices
            added-by = David Leverton <dleverton@exherbo.org>
            comment = lesspipe can use this; part of TeXLive, so might not be worth packaging separately
    hevea/
        :0 1.10
            homepage = http://pauillac.inria.fr/~maranget/hevea/
            description = Renders LaTeX files as HTML
            added-by = Robin Green <greenrd@greenrd.org>
            comment = Coq (proof assistant) needs this to build its docs
    jadetex/
        :0 3.13
            homepage = http://jadetex.sourceforge.net/
            description = TeX macro package for processing the output from Jade/OpenJade in TeX (-t) mode
            added-by = Artem Leschev <gallant.werewolf@ya.ru>
            comment = fontconfig needs this to build its docs
    jing/
        :0 20091111
            homepage = http://www.thaiopensource.com/relaxng/jing.html
            description = A RELAX NG validator in Java
            added-by = Heiko Becker <heirecka@exherbo.org>
            comment = media-libs/raptor needs it for some tests
    o3read/
        :0 0.0.4
            homepage = http://siag.nu/o3read/
            description = Standalone converter for the OpenOffice.org swriter and scalc formats
            added-by = David Leverton <dleverton@exherbo.org>
            comment = lesspipe can use this
    pstotext/
        :0 1.9
            homepage = http://pages.cs.wisc.edu/~ghost/doc/pstotext.htm
            description = Extracts plain text from PostScript
            added-by = David Leverton <dleverton@exherbo.org>
            comment = lesspipe can use this
    xlhtml/
        :0 0.5
            homepage = http://sourceforge.net/projects/chicago/
            description = Program for converting Microsoft Excel Files .xls
            added-by = David Leverton <dleverton@exherbo.org>
            comment = lesspipe can use this
dev-lang/
    gnat/
        :4.4 4.4.0
            homepage = http://gcc.gnu.org/
            description = GNAT Ada Compiler - gcc version
            added-by = Alex Elsayed <eternaleye@gmail.com>
            comment = Planning to use the GCC one exclusively, better-maintained
dev-libs/
    libchipcard/
        :0 4.2.7
            homepage = http://www2.aquamaniac.de/sites/libchipcard/
            description = Library for accessing chipcards
            added-by = Wulf C. Krueger <philantrop@exherbo.org>
            comment = Optional dep for GnuCash.
            remote-ids = freecode:libchipcard
    nacl/
        :0 2011.02.20
            homepage = http://nacl.cace-project.eu/
            description = A library for network communication, en-/decryption, etc
            added-by = Heiko Becker <heirecka@exherbo.org>
            comment = Optional dep for net-libs/libssh
    slirp/
        :0 1.0.17
            homepage = http://slirp.sourceforge.net/
            description = Emulates a PPP/SLIP connection over a normal terminal
            added-by = Wulf C. Krueger <philantrop@exherbo.org>
            comment = Optional dep for spice (network redirection support)
    tbb/
        :0 4.2_20131118
            homepage = https://www.threadingbuildingblocks.org/
            description = Intel® Threading Building Blocks lets you easily write parallel C++ programs
            added-by = Heiko Becker <heirecka@exherbo.org>
            comment = Optional dep for opencv (Threads)
    vc/
        :0 1.1.0
            homepage = https://github.com/VcDevel/Vc
            description = SIMD Vector Classes for C++
            added-by = Heiko Becker <heirecka@exherbo.org>
            comment = Optional dep for media-gfx/krita
gps/
    gypsy/
        :0 0.9
            homepage = https://gypsy.freedesktop.org/wiki/
            description = GPS multiplexing daemon allowing multiple clients to access GPS data
            added-by = Heiko Becker <heirecka@exherbo.org>
            comment = Optional dep of x11-libs/qtlocation
sys-analyzer/
    collectd/
        :0 5.0.0
            homepage = http://collectd.org/
            description = Pluggable system statistics collection daemon
            added-by = Nathan McSween <nwmcsween@gmail.com>
            comment = Modules could be handled via suboptions
sys-boot/
    splashutils/
        :0 1.5.4.3
            homepage = http://dev.gentoo.org/~spock/projects/gensplash/
            description = Boot-time splash and framebuffer console decorations
            added-by = Alex Elsayed <eternaleye@gmail.com>
www-client/
    weboob/
        :0 1.3
            homepage = http://weboob.org/
            description = A collection of applications able to interact with websites without browser
            added-by = Heiko Becker <heirecka@exherbo.org>
            comment = Optional dependency of finance/kmymoney
